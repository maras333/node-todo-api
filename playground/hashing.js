const {SHA256} = require('crypto-js');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

let message = 'I am user number 1';
let hash = SHA256(message).toString();

console.log(`Message: ${message}`);
console.log(`Hash: ${hash}`);


let data = {
  id: 333
}

let token = jwt.sign(data, 'secret');
console.log('token: ', token);
let decoded = jwt.verify(token, 'secret');
console.log('decoded: ', decoded);

// let data = {
//   id: 3
// }
// let token = {
//   data,
//   hash: SHA256(JSON.stringify(data) + 'secret').toString()
// }
//
// let resultHash = SHA256(JSON.stringify(token.data) + 'secret').toString();
// if(resultHash == token.hash) {
//   console.log('Data was not changed');
// } else {
//   console.log('Data was manipulated, do not trust.');
// }
