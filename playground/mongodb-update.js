const {
  MongoClient,
  ObjectID
} = require('mongodb');

MongoClient.connect('mongodb://localhost:27017/TodoApp', {
  useNewUrlParser: true
}, (err, client) => {
  if (err) {
    return console.log('Unable to connect to MongoDB server');
  }
  console.log('Connected to MongoDB server');

  const db = client.db('TodoApp');

  db.collection('Users').findOneAndUpdate({
      _id: new ObjectID('5ba9df5cd7211c123832325b')
    }, {
      $inc: {
        age: 2
      }
    }, {
      returnOriginal: false
    })
    .then((data) => {
      console.log(data);
    })
    .catch(e => {
      console.log(e);
    });


  // db.collection('Todos').findOneAndUpdate({
  //     _id: new ObjectID('5ba9de7104e7f611c0075f6e')
  //   }, {
  //     $set: {
  //       completed: true
  //     }
  //   }, {
  //     returnOriginal: false
  //   })
  //   .then((data) => {
  //     console.log(data);
  //   })
  //   .catch(e => {
  //     console.log(e);
  //   });

  // client.close();
});
