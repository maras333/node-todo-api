# Todo API in Node.js and MongoDB

## Run database server
* Create a data directory in the root folder of your app
* cd into your wherever you placed your mongo directory when you installed it
* run this command:
```
mongod --dbpath ~/path/to/your/app/data
```
## Run the app server
```
yarn install
yarn start
```
Run e.g. Postman and send requsets to API!