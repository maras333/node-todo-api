const mongoose = require('mongoose');
const validator = require('validator');
const jwt = require('jsonwebtoken');
const _ = require('lodash');
const bcrypt = require('bcryptjs');

let UserSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
    trim: true,
    minlength: 1,
    unique: true,
    validate: {
      validator: validator.isEmail,
      message: '{VALUE} is not an email'
    }
  },
  password: {
    type: String,
    required: true,
    minlength: 6
  },
  tokens: [{
    access: {
      type: String,
      required: true
    },
    token: {
      type: String,
      required: true
    }
  }]
})

UserSchema.methods.toJSON = function() {
  let user = this;
  let userObject = user.toObject();
  return _.pick(user, ['_id', 'email']);
}

UserSchema.methods.generateAuthToken = async function() {
  let user = this;
  let access = 'auth';
  let token = jwt.sign({
    _id: user._id.toHexString(),
    access
  }, process.env.JWT_SECRET).toString();

  user.tokens = user.tokens.concat([{
    access,
    token
  }]);

  user.save();
  return token;
}

UserSchema.methods.removeToken = function(token) {
  let user=this;
  return user.updateOne({
    $pull: {
      tokens: { token }
    }
  })
}

UserSchema.statics.findByToken = function(token) {
  let User = this;
  let decoded;
  try {
    decoded = jwt.verify(token, process.env.JWT_SECRET);
  } catch (e) {
    return Promise.reject();
  }
  return User.findOne({
    '_id': decoded._id,
    'tokens.token': token,
    'tokens.access': 'auth'
  });
}

UserSchema.statics.findByCredentials = async function(body) {
  let User = this;
  let email = body.email;
  let password = body.password;

  try {
    let user = await User.findOne({email});
    if(!user) {
      return Promise.reject('User not found');
    }
    return new Promise((resolve, reject) => {
      bcrypt.compare(password, user.password, (err, res) => {
        if(res) {
          resolve(user);
        } else {
          reject();
        }
      })
    })
  } catch (e) {
    return Promise.reject('Internal error');
  }

}

UserSchema.pre('save', async function(next) {
  let user = this;
  if (user.isModified('password')) {
    try {
      let salt = await bcrypt.genSalt(10);
      let hash = await bcrypt.hash(user.password, salt);
      user.password = hash;
      next();
    } catch (e) {
      console.log(e);
    }
  } else {
    next();
  }
});

let User = mongoose.model('User', UserSchema);

module.exports = {
  User
};
