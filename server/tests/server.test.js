const expect = require('expect');
const request = require('supertest');
const {ObjectID} = require('mongodb');

const {app} = require('./../server');
const {Todo} = require('./../models/todo');
const {User} = require('./../models/user');
const {todos, populateTodos, users, populateUsers} = require('./seed/seed');

beforeEach(populateUsers);
beforeEach(populateTodos);

describe('POST /todos', () => {
  it('should create a new todo', (done) => {
    let text = 'Test todo text';

    request(app)
      .post('/todos')
      .set('x-auth', users[0].tokens[0].token)
      .send({text})
      .expect(200)
      .expect((res) => {
        expect(res.body.text).toBe(text);
      })
      .end((err, res) => {
        if(err) {
          return done(err);
        }
        Todo.find({text})
         .then((todos) => {
           expect(todos.length).toBe(1);
           expect(todos[0].text).toBe(text);
           done();
         })
         .catch(e => done(e));
      })
  });

  it('should not create a new todo with invalid data', (done) => {

    request(app)
      .post('/todos')
      .set('x-auth', users[0].tokens[0].token)
      .send({})
      .expect(400)
      .end((err, res) => {
        if(err) {
          return done(err);
        }

        Todo.find()
         .then((todos) => {
           expect(todos.length).toBe(2);
           done();
         })
         .catch(e => done(e));
      })
  });

});

describe('GET /todos', () => {
  it('should get all todos', (done) => {
    request(app)
      .get('/todos')
      .set('x-auth', users[0].tokens[0].token)
      .expect(200)
      .expect((res) => {
        expect(res.body.todos.length).toBe(1);
      })
      .end((err, res) => {
        if(err) {
          return done(err);
        }
        done();
      })
  });
});

describe('GET /todos/:id', () => {
  it('should get todo with proper id', (done) => {
    request(app)
      .get(`/todos/${todos[0]._id.toHexString()}`)
      .set('x-auth', users[0].tokens[0].token)
      .expect(200)
      .expect((res) => {
        expect(res.body.todo.text).toBe(todos[0].text);
      })
      .end((err, res) => {
        if(err) {
          return done(err);
        }
        done();
      })
  });

  it('should not get todo with proper id but owned by someone else', (done) => {
    request(app)
      .get(`/todos/${todos[0]._id.toHexString()}`)
      .set('x-auth', users[1].tokens[0].token)
      .expect(404)
      .end(done);
  });

  it('should return 404 if todo not found', (done) => {
    let hexId = ObjectID().toHexString();
    request(app)
      .get(`/todos/${hexId}`)
      .set('x-auth', users[0].tokens[0].token)
      .expect(404)
      .end(done);
  });

  it('should return 404 for non object ids', (done) => {
    request(app)
      .get(`/todos/123`)
      .set('x-auth', users[0].tokens[0].token)
      .expect(404)
      .end(done);
  });
});

describe('DELETE /todos/:id', () => {
  it('should delete given todo', (done) => {
    let hexId = todos[1]._id.toHexString();
    request(app)
      .delete(`/todos/${hexId}`)
      .set('x-auth', users[1].tokens[0].token)
      .expect(200)
      .expect((res) => {
        expect(res.body.todo._id).toBe(hexId);
      })
      .end((err, res) => {
        if(err) {
          return done(err);
        }

        Todo.findById(hexId)
         .then((todo) => {
           expect(todo).toBeFalsy();
           done();
         })
         .catch(e => done(e));
      })
  });

  it('should not delete todo created by other user', (done) => {
    let hexId = todos[1]._id.toHexString();
    request(app)
      .delete(`/todos/${hexId}`)
      .set('x-auth', users[0].tokens[0].token)
      .expect(404)
      .end((err, res) => {
        if(err) {
          return done(err);
        }

        Todo.findById(hexId)
         .then((todo) => {
           expect(todo).toBeTruthy();
           done();
         })
         .catch(e => done(e));
      })
  });

  it('should return 404 if todo not found', (done) => {
    let hexId = ObjectID().toHexString();
    request(app)
      .delete(`/todos/${hexId}`)
      .set('x-auth', users[1].tokens[0].token)
      .expect(404)
      .end(done);
  });

  it('should return 404 for non object ids', (done) => {
    request(app)
      .delete(`/todos/123`)
      .set('x-auth', users[1].tokens[0].token)
      .expect(404)
      .end(done);
  });
});

describe('PUT /todos/:id', () => {
  it('should update given todo', (done) => {
    let hexId = todos[0]._id.toHexString();
    let newBody = {
      text: "New text",
      completed: true
    };
    request(app)
      .put(`/todos/${hexId}`)
      .set('x-auth', users[0].tokens[0].token)
      .send(newBody)
      .expect(200)
      .expect((res) => {
        expect(res.body.todo.text).toBe(newBody.text);
        expect(res.body.todo.completed).toBe(newBody.completed);
        expect(typeof res.body.todo.completedAt).toBe('number');
      })
      .end(done);
  });

  it('should not update todo created by someone else', (done) => {
    let hexId = todos[1]._id.toHexString();
    let newBody = {
      text: "New text",
      completed: true
    };
    request(app)
      .put(`/todos/${hexId}`)
      .set('x-auth', users[0].tokens[0].token)
      .send(newBody)
      .expect(404)
      .end(done);
  });

  it('should clear completedAt when todo is not completed', (done) => {
    let hexId = todos[1]._id.toHexString();
    let newBody = {
      completed: false
    };
    request(app)
      .put(`/todos/${hexId}`)
      .set('x-auth', users[1].tokens[0].token)
      .send(newBody)
      .expect(200)
      .expect((res) => {
        expect(res.body.todo.completed).toBe(newBody.completed);
        expect(res.body.todo.completedAt).toBeFalsy();
      })
      .end(done);
  });
});

describe('GET /users/me', () => {
  it('should return authenticated user', (done) => {
    request(app)
      .get(`/users/me`)
      .set('x-auth', users[0].tokens[0].token)
      .expect(200)
      .expect((res) => {
        expect(res.body._id).toBe(users[0]._id.toHexString());
        expect(res.body.email).toBe(users[0].email);
      })
      .end(done);
  });

  it('should return 401', (done) => {
    request(app)
      .get(`/users/me`)
      .expect(401)
      .expect((res) => {
        expect(res.body).toEqual({});
      })
      .end(done);
  });
});

describe('POST /users', () => {
  it('should create a user', (done) => {
    let email = 'testemail@example.com';
    let password = 'testowe';
    request(app)
      .post('/users')
      .send({email, password})
      .expect(200)
      .expect((res) => {
        expect(res.headers['x-auth']).toBeTruthy();
        expect(res.body._id).toBeTruthy();
        expect(res.body.email).toBe(email);
      })
      .end((err) => {
        if (err) return done(err);
        User.findOne({
            email
          })
          .then((user) => {
            expect(user.email).toBe(email);
            expect(user.password).not.toBe(password);
            done();
          })
          .catch(e => done(e));
      })

  });

  it('should return validation errors if request invalid', (done) => {
    let email = 'testemail@example';
    let password = 't';
    request(app)
      .post('/users')
      .send({email, password})
      .expect(400)
      .end(done);
  });

  it('should not create a user if email in use', (done) => {
    let email = 'one@example.com';
    let password = 'testowe';
    request(app)
      .post('/users')
      .send({email: users[0].email, password})
      .expect(400)
      .end(done);
  });
});

describe('POST /users/login', () => {
  it('should return 400 when invalid credentials', (done) => {
  let email = users[1].email;
  let password = users[1].password+'aaa';
  request(app)
    .post('/users/login')
    .send({
      email,
      password
    })
    .expect(400)
    .expect((res) => {
      expect(res.headers['x-auth']).toBeFalsy();
    })
    .end((err, res) => {
      if (err) return done(err);
      User.findById(users[1]._id)
        .then((user) => {
          expect(user.tokens.length).toBe(1);
          done();
        })
        .catch(e => done(e));
    })
  })

  it('login a user', (done) => {
    let email = users[1].email;
    let password = users[1].password;
    request(app)
      .post('/users/login')
      .send({
        email,
        password
      })
      .expect(200)
      .expect((res) => {
        expect(res.headers['x-auth']).toBeTruthy();
      })
      .end((err, res) => {
        if (err) return done(err);
        User.findById(users[1]._id)
          .then((user) => {
            expect(user.toObject().tokens[1]).toMatchObject({
              access: 'auth',
              token: res.headers['x-auth']
            });
            done();
          })
          .catch(e => done(e));
      })
  })
});

describe('DELETE /users/me/logout', () => {
  it('should remove token of user when logging out', (done) => {
    request(app)
      .delete('/users/me/token')
      .set('x-auth', users[0].tokens[0].token)
      .expect(200)
      .expect((res) => {
        expect(res.headers['x-auth']).toBeFalsy();
      })
      .end((err, res) => {
        if (err) return done(err);
        User.findById(users[0]._id)
          .then((user) => {
            expect(user.tokens.length).toBe(0);
            done();
          })
          .catch(e => done(e));
      })
  });

  it('should not logout user when logged out', (done) => {
    request(app)
      .delete('/users/me/token')
      .expect(401)
      .end((err, res) => {
        if (err) return done(err);
        User.findById(users[0]._id)
          .then((user) => {
            expect(user.tokens.length).toBe(1);
            done();
          })
          .catch(e => done(e));
      })
  });
});
