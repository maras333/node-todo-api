require('./config/config');
const express = require('express');
const bodyParser = require('body-parser');
const {ObjectID} = require('mongodb');
const _ = require('lodash');
const bcrypt = require('bcryptjs');


const {mongoose} = require('./db/mongoose');
const {Todo} = require('./models/todo');
const {User} = require('./models/user');
const {authenticate} = require('../server/middleware/authenticate');
const app = express();
const port = process.env.PORT;

app.use(bodyParser.json());

app.post('/todos', authenticate, async (req, res) => {
  let todo = new Todo({
    text: req.body.text,
    _creator: req.user._id
  })
  try {
    let doc = await todo.save();
    res.send(doc)
  } catch (e) {
    res.status(400).send(e);
  }
});

app.get('/todos', authenticate, async (req, res) => {
  try {
    let todos = await Todo.find({_creator: req.user._id});
    res.send({todos})
  } catch (e) {
    res.status(400).send();
  }
});

app.get('/todos/:id', authenticate, async (req, res) => {
  if(!ObjectID.isValid(req.params.id)) {
    res.status(404).send();
  }
  try {
    let todo = await Todo.findOne({
      _id: req.params.id,
      _creator: req.user.id
    });
    if(todo) {
      res.send({todo});
    } else {
      res.status(404).send();
    }
  } catch (e) {
    res.status(400).send();
  }
});

app.delete('/todos/:id', authenticate, async (req, res) => {
  if(!ObjectID.isValid(req.params.id)) {
    res.status(404).send();
  }
  try {
    let todo = await Todo.findOneAndDelete({
      _id: req.params.id,
      _creator: req.user.id
    })
    if(todo) {
      res.send({todo});
    } else {
      res.status(404).send();
    }
  } catch (e) {
    res.status(400).send();
  }
});

app.put('/todos/:id', authenticate, async (req, res) => {
  let id = req.params.id;
  let body = _.pick(req.body, ['text', 'completed']);

  if(!ObjectID.isValid(req.params.id)) {
    res.status(404).send();
  }

  if(_.isBoolean(body.completed) && body.completed) {
    body.completedAt = new Date().getTime();
  } else {
    body.completed = false;
    body.completedAt = null;
  }

  try {
    let todo = await Todo.findOneAndUpdate({
      _id: req.params.id,
      _creator: req.user._id
    }, {
      $set: body
    }, {
      new: true
    })
    if(!todo) {
      return res.status(404).send();
    }
    res.send({todo});
  } catch (e) {
    res.status(400).send();
  }
});

app.get('/users/me', authenticate, (req, res) => {
  res.send(req.user);
});

app.post('/users/login', async (req, res) => {
  let body = _.pick(req.body, ['email', 'password']);
  try {
    let user = await User.findByCredentials(body);
    let token = await user.generateAuthToken();
    res.header('x-auth', token).send({user});
  } catch (e) {
    res.status(400).send()
  }
});

app.delete('/users/me/token', authenticate, async (req, res) => {
  try {
    await req.user.removeToken(req.token);
    res.status(200).send();
  } catch (e) {
    res.status(400).send();
  }
});



app.post('/users', async (req, res) => {
  let body = _.pick(req.body, ['email', 'password']);
  let user = new User(body);
  try {
    await user.save();
    let token = await user.generateAuthToken();
    res.header('x-auth', token).send(user)
  } catch (e) {
    res.status(400).send(e);
  }
});

app.listen(port, () => {
  console.log(`Started on port ${port}`);
});

module.exports = {app};
